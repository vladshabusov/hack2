/* tslint:disable */
/* eslint-disable */
/**
 * Weather service
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface Weather
 */
export interface Weather {
    /**
     * 
     * @type {string}
     * @memberof Weather
     */
    date: string;
    /**
     * 
     * @type {string}
     * @memberof Weather
     */
    weather: string;
    /**
     * 
     * @type {string}
     * @memberof Weather
     */
    max: string;
    /**
     * 
     * @type {string}
     * @memberof Weather
     */
    min: string;
}

export function WeatherFromJSON(json: any): Weather {
    return WeatherFromJSONTyped(json, false);
}

export function WeatherFromJSONTyped(json: any, ignoreDiscriminator: boolean): Weather {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'date': json['date'],
        'weather': json['weather'],
        'max': json['max'],
        'min': json['min'],
    };
}

export function WeatherToJSON(value?: Weather | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'date': value.date,
        'weather': value.weather,
        'max': value.max,
        'min': value.min,
    };
}


