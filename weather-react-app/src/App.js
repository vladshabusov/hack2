import './App.css';
import React from 'react';
import WeeklyWeather from './components/WeeklyWeather';
import CurrentWeather from './components/CurrentWeather';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,  
  useLocation
} from "react-router-dom";
import moment from 'moment'
import WatchersPhotos from 'components/WatchersPhotos';

function App() {
  return (

<Router>
    <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/weather-today">Today's weather</Link>
            </li>
            <li>
              <Link to="/weather">Weather for the following week</Link>
            </li>
            <li>
              <Link to="/photos">Weather Watchers photos</Link>
            </li>
            <li>
              <Link to="/about">About our service</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
      <div className="App">
         
        <section> 
          <Switch>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/weather/:date">
              <CurrentWeather />
            </Route>
            <Route path="/weather-today">
              <Redirect to={`/weather/${moment().format('YYYY-MM-DD')}`} />
            </Route>
            
            <Route path="/weather">
              <WeeklyWeather />
            </Route>
            <Route path="/photos">
              <WatchersPhotos />
            </Route>
            <Route path="/">
              <h1>Home</h1>
              Welcome to our service. You can check weather <Link to="/weather">for a week</Link>, or <Link to="/weather-today">today's weather</Link>
            </Route>

            <Route path="*">
              <NoMatch />
            </Route>
            
            
          </Switch>
          

        </section>
      </div>
    </div>
    </Router>
  );
}
function NoMatch() {
  let location = useLocation();

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

function About() {
  let location = useLocation();

  return (
    <div>
      <h2>
        About us
      </h2>
      <p>Here is the description of the service and necessary terms.</p>
    </div>
  );
}
 
export default App;
