import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'


const api = new Api.DefaultApi()

class WatchersPhotos extends React.Component {

    constructor(props) {
        super(props);
        const date =  this.props.match?.params.date || moment().format('YYYY-MM-DD');
        console.log(date);

        

        this.state = { 
            photos: [],
            date: date 
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


    async handleReload(photos) {
        const response = await api.photos({ date: '2021-04-20'});
        this.setState({ photos: response });
    }


    render() {
        return <div>
            <h3>Our favourite Weather Watchers photos nearby</h3><br />
            <button onClick={this.handleReload}>Show new photos</button><br /><br />
            <div>
                {this.state.photos.map(
                    (photos) =>
                        <div><img src={photos.image} width="600px" height="400px" alt="place" /><br />
                            <b>{photos.city}</b>, {photos.country}<br />
                            {photos.weather}°C<br /><br /></div> )}
            </div>
        </div>
    }
}

export default withRouter(WatchersPhotos);
