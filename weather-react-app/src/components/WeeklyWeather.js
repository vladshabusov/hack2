import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import Moment from 'react-moment';
import moment from 'moment';


const api = new Api.DefaultApi()

class WeeklyWeather extends React.Component {

    constructor(props) {
        super(props);
        const dateParam = this.props.match?.params.id || moment().format('YYYY-MM-DD');
        const parsedDate = moment(dateParam, "YYYY-MM-DD")

        const nearestWeekend = parsedDate.startOf('week').isoWeekday(0);
        const endDate = moment(nearestWeekend).add(6, 'day');

        console.log("Enddate", nearestWeekend.toString(), endDate.toString())

        const startWeek = nearestWeekend.format("YYYY-MM-DD")
        const endWeek = endDate.format("YYYY-MM-DD")



        // TODO reuse data to service from https://citydog.by/post/play-musykanty/
        this.state = {
            weather: [],
            start: startWeek,
            end: endWeek

        };

        console.log(this.state.start, this.state.end)
        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


    async handleReload(weather) {
        const response = await api.weather({ date: this.state.startWeek });
        this.setState({ weather: response });
    }


    render() {
        return <div>
            <h2>Weekly weather</h2>
            <h3>Weather from <Moment format="YYYY/MM/DD">{this.state.start}</Moment> to <Moment format="YYYY/MM/DD">{this.state.end}</Moment> </h3>
            <h4>Choose city below:</h4>
            <div>
            <button onClick={this.handleReload}>Minsk</button>
            <button onClick={this.handleReload}>Moscow</button>
            <button onClick={this.handleReload}>London</button>
            <button onClick={this.handleReload}>Tokyo</button>
            </div><br />
            <div style={{display: "flex", flexDirection: "row", justifyContent: "space-around"}}>
                            <div>Sunday:</div>
                           <div> Monday:</div>
                           <div> Tuesday:</div>
                           <div> Wednesday:</div>
                           <div> Thursday:</div>
                            <div>Friday:</div>
                            <div>Saturday:</div>
                         </div><br />
            <div style={{display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
                {this.state.weather.map(
                    (weather) =>
                    <div>
                        
                        <div align="center" >max weather: {weather.max} °C, min weather: {weather.min} °C<br /> At that moment: {weather.weather} °C</div>
                    </div>)}
            </div>
        </div>
    }
}

export default withRouter(WeeklyWeather);