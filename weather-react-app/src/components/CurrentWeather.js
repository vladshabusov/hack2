import React from 'react';
import * as Api from 'typescript-fetch-api'
import moment from 'moment'
import Moment from 'react-moment';


const api = new Api.DefaultApi()

class CurrentWeather extends React.Component {

    constructor(props) {
        super(props);
        const date =  this.props.match?.params.date || moment().format('YYYY-MM-DD');
        console.log(date);

        

        this.state = { 
            day: [],
            date: date 
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


    async handleReload(day) {
        const response = await api.day({ date: '2021-03-25'/*this.state.targetDate*/ });
        this.setState({ day: response });
    }


    render() {
        return <div>
            {/*<button onClick={this.handleReload}>Reload</button> */}
            <h3>Weather on <Moment format="YYYY/MM/DD">{this.state.date}</Moment> </h3>
            <div>
            <button onClick={this.handleReload}>Minsk</button>
            <button onClick={this.handleReload}>Moscow</button>
            <button onClick={this.handleReload}>London</button>
            <button onClick={this.handleReload}>Tokyo</button>
            </div>
            <table align="center"><br />
               {this.state.day.map(
                   (day) => 
                        <tr ><td>
                            Max weather:<br />
                            Min weather:<br />
                            Morning:<br />
                            Afternoon:<br />
                            Evening:<br />
                            Night:
                        </td>
                        <td>
                            {day.max} °C<br />
                            {day.min} °C<br />
                            {day.one} °C<br />
                            {day.two} °C<br />
                            {day.three} °C<br />
                            {day.four} °C
                            </td>
                        </tr>)}
            </table>
        </div>
    }
}

export default CurrentWeather;
