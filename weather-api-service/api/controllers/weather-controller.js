'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');
const jsf = require('json-schema-faker');
const chance = require('chance')
const faker = require('faker')
jsf.extend('chance', () => new chance.Chance());
jsf.extend('faker', () => faker);

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */
module.exports = {
  weather: getWeather,
  day: getDay,
  photos: getPhotos
};

var schema = {
  "type": "array",
  "minItems": 7,
  "maxItems": 7,
  "items": {
    "type": "object",
    "required": [
      "date", "weather", "max", "min"
    ],
    "properties": {
      "date": {
        "type": "string",
        "faker": "date.recent"
      },
      "weather": {
        "type": "string",
        "chance": "minute"
      },
      "max": {
        "type": "string",
        "chance": "second"
      },
      "min": {
        "type": "string",
        "chance": "hour"
      }
    }
  }
}

var schemaPhotos = {
  "type": "array",
  "minItems": 3,
  "maxItems": 7,
  "items": {
    "type": "object",
    "required": [
      "image", "weather", "city", "country", "date"
    ],
    "properties": {
      "image": {
        "type": "string",
        "faker": "image.image"
      },
      "weather": {
        "type": "string",
        "chance": "hour"
      },
      "country": {
        "type": "string",
        "chance": "country"
      },
      "city": {
        "type": "string",
        "chance": "city"
      },
      "date": {
        "type": "string",
        "faker": "date.recent"
      }
    }
  }
}

/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
function getWeather(req, res) {
    // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
    var date = req.swagger.params.date.value || '2021-04-20';
  
    // this sends back a JSON response which is a single string
    jsf.resolve(schema).then(sample => res.json(sample));
}

function getDay(req, res) {
  var date = req.swagger.params.date.value || '2021-04-20';
  res.json([
    {
      "max": '' + faker.finance.amount(20,26,0),
      "date": date,
      "min": '' + faker.finance.amount(0,8,0),
      "one": '' + faker.finance.amount(2,9,0),
      "two": '' + faker.finance.amount(5,12,0),
      "three": '' + faker.finance.amount(9,26,0),
      "four": '' + faker.finance.amount(0,8,0)
    }
  ]);
}

function getPhotos(req, res) {
  var date = req.swagger.params.date.value || '2021-04-20';
  // this sends back a JSON response which is a single string
  jsf.resolve(schemaPhotos).then(sample => res.json(sample));
}